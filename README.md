
### What is this repository for? ###

This is part of the MATLAB code for the MDemodulator used by Scytale-C.

https://bitbucket.org/scytalec/scytalec-matlab

### Wiki ###

https://bitbucket.org/scytalec/scytalec-matlab/wiki/Home

### Who do I talk to? ###

* Repo owner or admin: microp11 at aelogic dot com
* Other community or team contact

### Licensing ###

GNU General Public License 3, microp11 2017
 
Scytale-C is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Scytale-C is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

See http://www.gnu.org/licenses/.