%
%  microp11 2017
%  
%  This file is part of Scytale-C.
%  https://bitbucket.org/scytalec/scytalec
%  
%  Scytale-C is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  Scytale-C is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
%
%
%  BPSK demodulator
%  
%  Otti and Jonti, many thanks for help and encouragement!
%  I would not have thought that such a beauty and complexity exists
%  in the things we never see. And I could not have written this code
%  and enjoy the amazement within without your help.
%  Thank you.
%
%  Bibliography:
%    
%  Digital Communications Playlists
%  Bernd Porr, UofG
%  https://www.youtube.com/channel/UCeFwNKTf19bJL1K5yYTpdqg/playlists
% 
%  bpsk encoder/decoder using java audio 
%  f4grx/jbpsk 
%  https://github.com/f4grx/jbpsk
% 
%  Design and Implementation of High Performance BPSK Demodulator for Satellite Communications
%  by Tofigh, E.
%  MS Thesis, TUDelft
%  https://repository.tudelft.nl/islandora/object/uuid%3Ac102b61b-5bad-4550-9dbb-33ca2e191bd9
% 
%  Practical Costas loop design - Designing a simple and inexpensive BPSK Costas loop carrier recovery circuit
%  Feigin, J. 
%  in RF DESIGN; 25; 20-37; RF DESIGN
%  by CARDIFF PUBLISHING COMPANY INC; 2002
% 
%  JDSKA, DSCA demodulator and decoder written in C++ Qt
%  jontio/JDSCA 
%  http://jontio.zapto.org/hda1/jdsca.html 
%  https://github.com/jontio/JDSCA
%    
%  Telecommunication Breakdown: Concepts of Communication Transmitted via Software-Defined Radio
%  by C. R. Johnson, Jr. and W. A. Sethares
%  http://sethares.engr.wisc.edu/telebreak.html
%  
%  Software Defined Radio Using MATLAB & Simulink and the RTL-SDR
%  by K. Barlee, University of Strathclyde, D. Atkinson, University of Strathclyde, R.W. Stewart, University of Strathclyde
%  L. Crockett, University of Strathclyde
%  ISBN: 978-0-9929787-1-6
%  http://www.desktopsdr.com/
%         
%  Communication for Beginners in Matlab: BPSK Modulation Demodulation With AWGN Channel
%  rupam rupam
%  https://www.youtube.com/watch?v=B4TFTT1rnYc
%  
%  Gaussianwaves.com - Signal Processing Simplified
%  http://www.gaussianwaves.com/2013/11/symbol-timing-recovery-for-qpsk-digital-modulations/
%  
%  WinPSK
%  http://www.moetronix.com/ae4jy/winpsk.htm
%  
%  David Dorran Playlists and Matlab
%  https://www.youtube.com/user/ddorran/playlists
%  https://dadorran.wordpress.com/
%    
%

clear all;

[samples,fs]=audioread('bpsk_input.wav', 'native');

%convert to single, as this is what we'll have in C#
samples1 = single(samples);

%pseudo-normalization (pn), this is what we'll use in C#
magnitude = 0.0;
 
for ii = 1:length(samples)
    magnitude = magnitude + abs(samples1(ii));
end
mean_abs_magnitude = magnitude/length(samples1);

for ii = 1:length(samples1)
    pn_samples(ii) = samples1(ii)/mean_abs_magnitude;
end

figure(1);
%plot (samples1);
%hold on;
plot (pn_samples, '-')
xlabel('Samples');
ylabel('Amplitude');
hold off;

freq = 2000;
SR = 48000;
SymbolRate = 1200;
Ts = SR/SymbolRate;
alpha = 0.0035;
beta = alpha*alpha/4;
phase = 0;
omega = 2*pi*freq/SR;
error = 0;

% LPF1, LPF2, here determined by experiment
% The literature suggestes that the square-shaped LPF filter should have a
% banwidth of no less than half the symbol rate. This will remove the most
% noise possible without reducing the amplitude of the desired signal at
% its sampling instants.
% [b,a] = butter(5, 0.89, 'low');

% I have tried different combinations of frequencies and factors, 1 and 0.5
% seems to be the best so far with no noticeable improvent over the 5,
% 0.89, which means that the next thing to check is the normalized
% frequencies that pass through theses filters
[b,a] = butter(1, 0.5, 'low');

% The literature I found indicates that the loop filter should have a
% response far outside the LPF1/LPF2. The fastest achievable settle
% time is one in which the VCO has a gain 8x that of the LPF1/LPF2 pole
% frequency. For LPF3, a factor of six times K or 12 times the LPF1/LPF2
% pole is a better choice

% however this will give the same filter result as the LPF1 and LPF2
% so it is commented out and I use the same fir filter
% % [z,p,k] = tf2zp(b,a);
% % z1 = z;
% % p1 = p.*12;
% % k1 = k;
% % [b1,a1] = zp2tf(z1,p1,k1);

% filter initial and final condition to assist filtering in chunks
zfssI = zeros(max(length(a),length(b))-1,1);
zfssQ = zeros(max(length(a),length(b))-1,1);

zfssLoop = zeros(max(length(a),length(b))-1,1);

%the RRC filters are currently used outside of the loop, I have tested them
%when they are inside the loop as well. for the simulation they will be
%used as in here.

%RRC1
SPS        = 40;               % Samples per Symbol
PulseShape = 'Raised Cosine';  % Pulse shape
Astop      = 50;               % Stopband Attenuation
Beta       = 1;                % Rolloff Factor
h = fdesign.pulseshaping(SPS, PulseShape, 'ast,beta', Astop, Beta);

Hd = design(h, 'window');
Hd.PersistentMemory = true;
Hd.States = 1;

%RRC2
%h1 = fdesign.pulseshaping(SPS, PulseShape, 'ast,beta', Astop, Beta);
Hd1 = design(h, 'window');
Hd1.PersistentMemory = true;
Hd1.States = 1;

%costas
for ii = 1:length(samples)
   phase = phase + omega + alpha*error;
   omega = omega + beta*error;
   
   freq = (omega*SR)/(2*pi);
   
   %for display only
   f(ii) = freq;
   
   if (freq < 1750)
       freq = 1750;
       omega = 2*pi*freq/SR;
   end
   
   if (freq > 2350)
       freq = 2350;
       omega = 2*pi*freq/SR;
   end
   
   if (phase > 2*pi)
       phase = phase - 2*pi;
   end
   
   % vco
   sI = cos(phase);
   sQ = -sin(phase);
   
   % mixer
   tsIm = sI * pn_samples(ii);
   tsQm = sQ * pn_samples(ii);
   
   %convert to complex and rotate the tsQm branch, see if we can minimize the
   %q in the error signal
   bbSigOut = tsIm + 1i*tsQm;
      
   %rotate
   rotation = 0.059;
   bbSigOut = bbSigOut * exp(1i*2*pi*rotation);

   sIm = real(bbSigOut);
   sQm = imag(bbSigOut);

   % LPF1, LPF2 mixer
   [sIm, zfssI] = filter(b,a,sIm, zfssI);
   [sQm, zfssQ] = filter(b,a,sQm, zfssQ);

   % vco error without gain
   error = sIm*sQm;
   
   %LPF loop
   [error, zfssLoop] = filter(b,a, error, zfssLoop);   
      
   %save values for display purposes
   sim(ii) = sIm;
   sqm(ii) = sQm;
%    err(ii) = error;
end
 
figure(2);
plot(f);
xlabel('Samples');
ylabel('Frequency');
hold on;

%figure(2);
%plot(sqm);
%hold on;
%plot(err);
%plot(sim);

% figure(3);
% plot(sim);
% xlabel('Samples');
% ylabel('Amplitude');

% this will also have to be make to work in chunks
% hold on;
%RRC
y_sim = filter(Hd, sim);
y_sqm = filter(Hd1, sqm);

% %y = rrc1(sim)*10;
% k = 1;
% % RRC corrected for filter delay
% for ii= 1 : length(y_sim)
%     if (ii > 120)
%         yy(k) = y_sim(ii);
%         k = k + 1;
%     end
% end

% plot(yy);

%gardner

symbol_2xtimer = 0;
samples_ago = 0;
Ts = 40;
is_on_point = 1;
sample_last_on = 0;
aggression = 1.5;
output = [];
output_time = [];
max_error_x_aggresion = 0;
figure(3);

for i = 1: length(y_sim)
    %these are samples after the carrier tracking and the RRC filter
    %current_sample = data_re(i)+1i*data_im(i);
    current_sample = y_sim(i) + 1i*y_sqm(i);

    %sample at 2x bit rate
    symbol_2xtimer = symbol_2xtimer + 2;
    
    %simple way to avoid double sampling due to timing jitter
    samples_ago = samples_ago + 1;
    
    if(symbol_2xtimer >= Ts && samples_ago > max_error_x_aggresion + 1)%T/4)% T/4 or something bigger than the biggest jump that aggression*error can do
        symbol_2xtimer = symbol_2xtimer - Ts;
        samples_ago = 0;
        is_on_point = 1 - is_on_point;

        if(is_on_point)%we are on time here
            sample_this_on = current_sample;

            %calculate carrier timing error
            error = (sample_this_on - sample_last_on) * sample_off;
            
            %adjust carrier aggression
            max_error_x_aggresion = max(max_error_x_aggresion, error * aggression);

            %steer symbol timing
            symbol_2xtimer = symbol_2xtimer + error * aggression;

            %save on symbol
            output = [output sample_this_on];
            output_time = [output_time i];
            
            %save this on time for next on time
            sample_last_on = sample_this_on;
            
        % we are off time here
        else
            sample_off = current_sample;
        end
    end
end

% plot the gardner vs samples
data1 = y_sim + 1i*y_sqm;
data1(1) = 3;
data1(2) = -3;
for i=1:length(output_time)
%     if (data1(output_time(i)) > 0)
%         data1(output_time(i)) = 2;
%     else
%         data1(output_time(i)) = -2;
%     end
    data1(output_time(i)) = output(i)*2;
end

% REPRESENTS output vs. RRC-ed samples
figure(3);
stem(real(data1));
hold on;
stem(real(y_sim));

% % using the gardner object in Matlab for verification
% SamplesPerSymbol = 48000/1200;
% Hg = comm.SymbolSynchronizer('TimingErrorDetector', 'Gardner (non-data-aided)', 'SamplesPerSymbol', SamplesPerSymbol);
% 
% [sym1, phase1] = Hg(y_sim');
% figure(4);
% stem(sym1);

%get hard bits for comparison to c# results; ambigous/inverted
output_bin = [];
for i=1:length(output)
    if real(output(i)) > 0
        output_bin = [output_bin 1];
    else
        output_bin = [output_bin 0];
    end
end

% header1 = 'Hard bits';
% fid=fopen('bin-inmarsat-9970-9971.txt','w');
% fprintf(fid, [ header1 'r\n']);
% fprintf(fid, '%d', [output_bin]');
% fclose(fid);

figure(4);
sti = length(output) - 300;
scatter(real(output(sti:length(output))), imag(output(sti:length(output))));
xlim([-1 1]);
ylim([-1 1]);
